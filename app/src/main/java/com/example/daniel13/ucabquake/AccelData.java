package com.example.daniel13.ucabquake;

/**
 * Created by Daniel13 on 4/3/2017.
 */

public class AccelData {

    private String timestamp;
    private double data;

    public AccelData(double data) {
        this.data = data;

    }

    public AccelData(String timestamp) {
        this.timestamp = timestamp;

    }
    public String getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public double getData() {
        return data;
    }
    public void setData(double x) {
        this.data = data;
    }


    public String toString()
    {
        return String.valueOf(data);
    }
}
