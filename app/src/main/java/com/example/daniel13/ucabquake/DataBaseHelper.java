package com.example.daniel13.ucabquake;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel13 on 4/20/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = DataBaseHelper.class.getSimpleName();

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "UcabQuake.db";

    // Table Names
    private static final String TABLE_REGISTRO = "Registros";




    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String SQL_CREATE_REGISTRO_TABLE =  "CREATE TABLE " + RegistroContract.RegistroEntry.TABLE_NAME + " ("
                + RegistroContract.RegistroEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + RegistroContract.RegistroEntry.COLUMN_FECHA + " DATETIME NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_HORA + " DATETIME NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_X + " REAL NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_Y + " REAL NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_Z + " REAL NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_LATITUD + " REAL NOT NULL, "
                + RegistroContract.RegistroEntry.COLUMN_LONGITUD + " REAL NOT NULL);";


        sqLiteDatabase.execSQL(SQL_CREATE_REGISTRO_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + TABLE_REGISTRO);
        onCreate(sqLiteDatabase);


    }


}
