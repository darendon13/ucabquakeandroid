package com.example.daniel13.ucabquake.Objetos;

import java.util.Date;

/**
 * Created by Daniel13 on 5/3/2017.
 */

public class Registro {

    double x;
    double y;
    double z;
    String fecha;
    String hora;

    public Registro(float x, float y, float z, Date curDate, Date curTime) {
    }

    public Registro(double x, double y, double z,String fecha, String hora) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.fecha = fecha;
        this.hora = hora;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
}
