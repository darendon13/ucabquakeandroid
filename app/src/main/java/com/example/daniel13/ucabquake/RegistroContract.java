package com.example.daniel13.ucabquake;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.Date;

/**
 * Created by Daniel13 on 4/21/2017.
 */

public class RegistroContract {

    public RegistroContract() {
    }

    public static final String CONTENT_AUTHORITY = "com.example.daniel13.ucabquake";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_REGISTROS = "registros";

    public static final class RegistroEntry implements BaseColumns{

        /** The content URI to access the pet data in the provider */
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_REGISTROS);

        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REGISTROS;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REGISTROS;

        public final static String TABLE_NAME = "registros";

        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_FECHA ="fecha";

        public final static String COLUMN_HORA ="hora";

        public final static String COLUMN_X ="x";

        public final static String COLUMN_Y ="y";

        public final static String COLUMN_Z ="z";

        public final static String COLUMN_LATITUD ="latitud";

        public final static String COLUMN_LONGITUD ="longitud";


    }
}
