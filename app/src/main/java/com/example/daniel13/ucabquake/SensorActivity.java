package com.example.daniel13.ucabquake;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.FloatingActionButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.daniel13.ucabquake.Objetos.FirebaseReferences;
import com.example.daniel13.ucabquake.Objetos.Registro;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SensorActivity extends Activity implements SensorEventListener {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ArrayList sensorDataX;
    private ArrayList sensorDataY;
    private ArrayList sensorDataZ;
    private ArrayList sensorDataT;
    private ArrayList sensorDataD;
    GPSTracker gps;
    private static final int REQUEST_CODE_PERMISSION = 2;
    // Database Helper
    DataBaseHelper dataBaseHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        dataBaseHelper = new DataBaseHelper(this);
        //FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().subscribeToTopic("Registro_Notifications");



        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_PERMISSION);

                // If any permission above not allowed by user, this condition will

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SensorActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });



        sensorDataT = new ArrayList();
        sensorDataD = new ArrayList();
        sensorDataX = new ArrayList();
        sensorDataY = new ArrayList();
        sensorDataZ = new ArrayList();
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        if ((ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {


            if ((ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE))) {


            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);

            }
        }


        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }

                return;
            }
        }

    }

    public void on50000(View view){
        mSensorManager.registerListener(this, mAccelerometer, 50000);
    }
    public void on100000(View view){
        mSensorManager.registerListener(this, mAccelerometer, 100000);
    }


    public void onStartClick(View view) {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }

    public void onStopClick(View view) {

        mSensorManager.unregisterListener(this);



    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(FirebaseReferences.TESIS_REFERENCE);


        // create class object
        gps = new GPSTracker(this);

        // check if GPS enabled
        if(gps.canGetLocation()){



            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            // \n is for new line
            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: "
                    + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            Log.v("Long", String.valueOf(longitude));
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }



        // TODO Auto-generated method stub
        TextView tvX = (TextView) findViewById(R.id.x_axis);
        TextView tvY = (TextView) findViewById(R.id.y_axis);
        TextView tvZ = (TextView) findViewById(R.id.z_axis);
        TextView tvt = (TextView) findViewById(R.id.time);
        TextView tvd = (TextView) findViewById(R.id.date);


        SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss.SSS");
        Date curTime = new Date(System.currentTimeMillis());
        String horaAcc = formatterTime.format(curTime);

        SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy");
        Date curDate = new Date(System.currentTimeMillis());
        String fechaAcc = formatterDate.format(curDate);


        float x = sensorEvent.values[0];
        float y = sensorEvent.values[1];
        float z = sensorEvent.values[2];

        AccelData dataX = new AccelData(x);
        AccelData dataY = new AccelData(y);
        AccelData dataZ = new AccelData(z);

        sensorDataX.add(dataX);
        sensorDataY.add(dataY);
        sensorDataZ.add(dataZ);
        sensorDataT.add(horaAcc);
        sensorDataD.add(fechaAcc);

        Registro registro = new Registro(x,y,z,fechaAcc,horaAcc);
        //myRef.child(FirebaseReferences.REGISTRO_REFERENCE).push().setValue(registro);
        myRef.push().setValue(registro);

        ContentValues values = new ContentValues();
        values.put(RegistroContract.RegistroEntry.COLUMN_FECHA, String.valueOf(curDate));
        values.put(RegistroContract.RegistroEntry.COLUMN_HORA, String.valueOf(curTime));
        values.put(RegistroContract.RegistroEntry.COLUMN_X, x);
        values.put(RegistroContract.RegistroEntry.COLUMN_Y, y);
        values.put(RegistroContract.RegistroEntry.COLUMN_Z, z);
        values.put(RegistroContract.RegistroEntry.COLUMN_LATITUD, gps.getLatitude());
        values.put(RegistroContract.RegistroEntry.COLUMN_LONGITUD, gps.getLongitude());

        //long newrowID = db.insert(RegistroContract.RegistroEntry.TABLE_NAME,null, values);


        if (!mInitialized) {
            //mLastX = x;
            // mLastY = y;
            // mLastZ = z;
            tvX.setText("0.0");
            tvY.setText("0.0");
            tvZ.setText("0.0");
            tvt.setText("0.0");
            mInitialized = true;
        } else {


            //  listX.add(Float.toString(x));


            tvX.setText(Float.toString(x));
            tvY.setText(Float.toString(y));
            tvZ.setText(Float.toString(z));
            tvt.setText(horaAcc);
            tvd.setText(fechaAcc);

            if (isWriteable()) {

                writePrueba(sensorDataX,sensorDataY,sensorDataZ,sensorDataT,sensorDataD);
            }


        }
    }

    public void writePrueba(ArrayList sensorDataX,ArrayList sensorDataY,ArrayList sensorDataZ,ArrayList sensorDataT,ArrayList sensorDataD){
        try
        {
            File ruta_sd = Environment.getExternalStorageDirectory();

            File f = new File(ruta_sd, "RegistroUcabquake.txt");


            OutputStreamWriter fout =
                    new OutputStreamWriter(
                            new FileOutputStream(f));

            fout.write("X = " + String.valueOf(sensorDataX)+ "\n");
            fout.write("\n");
            fout.write("Y = " + String.valueOf(sensorDataY)+ "\n");
            fout.write("\n");
            fout.write("Z = " + String.valueOf(sensorDataZ) + "\n");
            fout.write("\n");
            fout.write("Hora = " + sensorDataT + "\n");
            fout.write("\n");
            fout.write("Hora = " + sensorDataD + "\n");
            fout.close();
            Toast.makeText(getBaseContext(),
                    "Archivo creado",
                    Toast.LENGTH_SHORT).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(getBaseContext(), ex.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isWriteable(){
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


}

